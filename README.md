
# Radiant

Radiant is a c++ library that bakes lightmaps. it is depreived from the LMB library.
The library is flexible and extendable and it also supports multithreading.
Radiant supports direct lighting, ambient occlusion,
indirect lighting, denoising and more to come in the future.You can also extend the features on your own fairly easily.

