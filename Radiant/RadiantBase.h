#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
*/

//Basic Includes
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstdio>
#include <cstdint>
#include <thread>   
#include <ctime>
#include <map>
#include <list>
#include <algorithm>
#include <memory>
#include <string>
#include <vector>
#include<functional>
#include <thread>
#include <list>
#include <chrono>
#include <deque>
#include <queue>
#include <combaseapi.h>
//Engine Defines

namespace Radiant {
	class RadiantSession
	{
	public:
		RadiantSession();
		~RadiantSession();
		enum class SessionState {
			SS_TRIANGLE = 1,
			SS_SOLVER = 2,
			SS_CALC = 3,
		};
	private:
	protected:

	};

	RadiantSession::RadiantSession()
	{
	}

	RadiantSession::~RadiantSession()
	{
	}
}